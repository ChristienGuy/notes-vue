import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from './components/Auth.vue'
import NotesEditor from './components/NotesEditor.vue'
// import App from './App'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: NotesEditor },
  { path: '/auth', component: Auth },
  { path: '*', redirect: '/' }
]

export default new VueRouter({
  mode: 'history',
  routes
})
