// store.js

import Vue from 'vue'
import Vuex from 'vuex'
import VuexFire from 'vuexfire'

Vue.use(Vuex)

// The root, initial state object
const state = {
  notes: [],
  user: {},
  activeNote: {},
  totalNotes: 0,
  signInModalShown: false
}

// Define the possible mutations that can be applied to state
// Think of mutations as the only means to change state
const mutations = {
  EDIT_NOTE (state, text) {
    state.activeNote.text = text
  },

  SET_ACTIVE_NOTE (state, note) {
    state.activeNote = note
  },

  TOGGLE_FAVORITE (state) {
    state.activeNote.favorite = !state.activeNote.favorite
  },

  SET_USER (state, user) {
    state.user = user
  },

  SET_FB_APP (state, fbApp) {
    state.fbApp = fbApp
  },

  SET_FB_UI_APP (state, fbUiApp) {
    state.fbUiApp = fbUiApp
  },

  SHOW_SIGN_IN_MODAL (state) {
    state.signInModalShown = true
  }
}

const actions = {
  updateActiveNote ({ commit }, note) {
    commit('SET_ACTIVE_NOTE', note)
  },
  editNote ({ commit }, e) {
    commit('EDIT_NOTE', e.target.value)
  },
  toggleFavorite ({ commit }) {
    commit('TOGGLE_FAVORITE')
  },
  showSignInModal ({ commit }) {
    commit('SHOW_SIGN_IN_MODAL')
  },
  setUser ({ commit }, user) {
    commit('SET_USER', user)
  }
}

// Define the actions that will initiate mutators the modify state.
// Mutators must be sychronous but we can run actions asychronously.

const getters = {
  notes: state => state.notes,
  activeNote: state => state.activeNote,
  activeNoteText: state => state.activeNote.text,
  user: state => state.user,
  users: state => state.users,
  signInModalShown: state => state.signInModalShown
}

// Create the vuex instance by combining our state and mutations objects
// then export the Store for use in components
export default new Vuex.Store({
  state,
  mutations: {
    ...VuexFire.mutations,
    ...mutations
  },
  actions,
  getters
})
