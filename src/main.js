// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VuexFire from 'vuexfire'
import store from './vuex/store'
import router from './router'
import { fbApp } from './data/auth'

Vue.use(VuexFire)

const db = fbApp.database()

/* eslint-disable no-new */
new Vue({
  created () {
    fbApp.auth().onAuthStateChanged(user => {
      if (user) {
        this.$bindAsArray('notes', db.ref(`${user.uid}/notes`))
      }
    })
  },
  store,
  router,
  firebase: {
  },
  render: h => h(App)
}).$mount('#app')
