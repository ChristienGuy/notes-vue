var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  FB_API_KEY: '"AIzaSyCAvO5ILXwYjmvWoFHjKZHcn-KPVIUhy60"',
  FB_AUTH_DOMAIN: '"notes-974fe.firebaseio.com"',
  FB_DATABASE_URL: '"https://notes-974fe.firebaseio.com"',
  FB_STORAGE_BUCKET: '"gs://notes-974fe.appspot.com"',
  FB_MESSAGING_SENDER_ID: '""'
})
